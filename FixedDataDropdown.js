// $Id $
// Author: Mark Burton

// its ok to have one per page, as the user cant put the mouse on 2 different
// autocompletes at once...
var fieldid;
function fdd_updater() 
{
          selection=$('.fixedDataDropdown_selector').filter('[@id*='+fieldid+']').val();
          
          $('.fixedDataDropdown_value').filter('[@id*='+fieldid+']').removeOption(/./).ajaxAddOption("?q=/fixedDataDropdown/autocomplete/"+fieldid+"/"+selection);
}
var t;

if (Drupal.jsEnabled) {
  $(document).ready(
    function() {
      $('.fixedDataDropdown_selector').keyup (
        function() {
          fieldid=this.id;
          clearTimeout(t);
          t=setTimeout("fdd_updater()",800);
        });
      
    });
  
};

